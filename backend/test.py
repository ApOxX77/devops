import unittest

class TestSimpleAssertions(unittest.TestCase):
    def test_true(self):
        self.assertTrue(True, "La valeur n'est pas vraie")

    def test_false(self):
        self.assertFalse(False, "La valeur n'est pas fausse")

    def test_equal(self):
        self.assertEqual(5, 5, "Les valeurs ne sont pas égales")

    def test_not_equal(self):
        self.assertNotEqual(3, 7, "Les valeurs sont égales")

    def test_greater(self):
        self.assertGreater(7, 5, "7 n'est pas supérieur à 5")

    def test_less(self):
        self.assertLess(3, 5, "3 n'est pas inférieur à 5")

    def test_in(self):
        fruits = ['pomme', 'banane', 'orange']
        self.assertIn('pomme', fruits, "La pomme n'est pas dans la liste de fruits")

    def test_not_in(self):
        fruits = ['pomme', 'banane', 'orange']
        self.assertNotIn('raisin', fruits, "Le raisin est dans la liste de fruits")

if __name__ == '__main__':
    unittest.main()
